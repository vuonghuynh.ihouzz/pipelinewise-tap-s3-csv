FROM python:3.9.7-slim-buster

RUN apt update && apt install -y gcc

RUN pip install pipelinewise-tap-s3-csv==1.2.2

ENTRYPOINT ["tap-s3-csv"]
